# tiny_engine

C++ engine for generating fantasy Characters from the game Badlur's Gate II, this uses a
step builder pattern, similar to a
[builder pattern](https://en.wikipedia.org/wiki/Builder_pattern),
however it prevents you from building until all parts of
the character are supplied.

See Main for an example as to how to build a character correctly.

We are going to be as true as possible to the Baldur's Gate game, so Races
play a role in determining what Classes your character can be.
For example dwarves can not be Druids.

## Building

Use you favorite c++ compiler

```bash
g++ Main.cpp
```

```bash
clang++ Main.cpp
```

## Running
```bash
rm -f a.out && g++ Main.cpp && ./a.out
```

### Contributing

Need to install [clang-format](https://electronjs.org/docs/development/clang-format), this is our code formatter.
