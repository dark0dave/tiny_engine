#include <functional>
#include <iostream>

#include "Dice/Dice.h"
#include "Character/CharacterStepBuilder.h"

int main() {
  const Character *one = CharacterStepBuilder()
                             .setName("Aragon")
                             .setRace(ValidRace::Human)
                             .setCharacterClass(ValidClass::Fighter)
                             .generate(1, 1, 1, 1, 1, 1, 1)
                             .build();

  const Character *two = CharacterStepBuilder()
                             .setName("Legolas")
                             .setRace(ValidRace::Elf)
                             .setCharacterClass(ValidClass::Ranger)
                             .generate(1, 1, 1, 1, 1, 1, 1)
                             .build();

  std::cout << "Character one health: " << one->stats.health << "\n";
  std::cout << "Character two health: " << two->stats.health << "\n";
  return 0;
}