#include <chrono>
#include <cmath>
#include <random>

std::mt19937 mt_rand(
    std::chrono::steady_clock::now().time_since_epoch().count());

int roll(int side) { return 1 + abs(mt_rand()) / (INT32_MAX / side); }

int d4() { return roll(4); }

int d6() { return roll(6); }

int d8() { return roll(8); }

int d10() { return roll(10); }
