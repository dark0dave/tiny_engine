class Character {
 public:
  Stats &stats;
  std::string name;
  Race &race;
  CharacterClass &characterClass;

  Character(Stats &stats, std::string &name, Race &race,
            CharacterClass &characterClass)
      : stats(stats), name(name), race(race), characterClass(characterClass){};
};
