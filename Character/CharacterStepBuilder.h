#include "../Stats/Stats.h"
#include "../Races/ValidRace.h"
#include "../CharacterClasses/ValidClass.h"
#include "../Races/RaceFactory.h"
#include "../CharacterClasses/CharacterClassFactory.h"
#include "Character.h"
#include "Steps.h"

/*
  The order in which to call the methods is as follows:
    setName,
    setRace,
    setCharacterClass,
    generate,
    and finally build, which generates a character
*/
class CharacterStepBuilder : NameStep,
                             RaceStep,
                             CharacterClassStep,
                             StatsStep,
                             BuildStep {
 private:
  Stats &stats;
  std::string name;
  Race &race;
  CharacterClass &characterClass;

 public:
  RaceStep &setName(const std::string name_) {
    name = name_;
    return *this;
  }

  CharacterClassStep &setRace(const ValidRace choice) {
    race = *RaceFactory::GenerateRace(choice);
    return *this;
  }

  StatsStep &setCharacterClass(const ValidClass choice) {
    characterClass =
        *CharacterClassFactory::GenerateCharacterClass(choice, race);
    return *this;
  }

  BuildStep &generate(int charisma, int constitution, int dexterity,
                      int intelligence, int strength, int wisdom, int level) {
    stats = Stats(charisma, constitution, dexterity, intelligence, strength,
                  wisdom, level, characterClass.healthPerLevel(level));

    race.modifyStats(stats);

    return *this;
  }

  Character *build() {
    return new Character(stats, name, race, characterClass);
  }

  CharacterStepBuilder()
      : stats(*new Stats()),
        name(""),
        race(*new Human()),
        characterClass(*new Fighter()){};

  virtual ~CharacterStepBuilder(){};
};
