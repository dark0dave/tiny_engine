/*
  These are the virtual build Classes
  for the character step builder,
  they return the class which is the next
  step in the build process
*/

class BuildStep {
 public:
  virtual Character *build() = 0;

 protected:
  ~BuildStep(){};
};

class StatsStep {
 public:
  virtual BuildStep &generate(int charisma, int constitution, int dexterity,
                              int intelligence, int strength, int wisdom,
                              int level) = 0;

 protected:
  ~StatsStep(){};
};

class CharacterClassStep {
 public:
  virtual StatsStep &setCharacterClass(const ValidClass choice) = 0;

 protected:
  ~CharacterClassStep(){};
};

class RaceStep {
 public:
  virtual CharacterClassStep &setRace(const ValidRace choice) = 0;

 protected:
  ~RaceStep(){};
};

class NameStep {
 public:
  virtual RaceStep &setName(const std::string name) = 0;

 protected:
  ~NameStep(){};
};
