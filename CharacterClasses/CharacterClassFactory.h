#include "CharacterClass.h"
#include "CharacterClasses.h"

class CharacterClassFactory {
 public:
  static CharacterClass *GenerateCharacterClass(const ValidClass choice,
                                                Race &race) {
    if (race.availableClasses.count(choice) == 0) {
      throw std::invalid_argument("Illegal combination class race combination");
    }

    switch (choice) {
      case ValidClass::Fighter:
        return new Fighter();
      case ValidClass::Ranger:
        return new Ranger();
      case ValidClass::Paladin:
        return new Paladin();
      case ValidClass::Cleric:
        return new Cleric();
      case ValidClass::Druid:
        return new Druid();
      case ValidClass::Monk:
        return new Monk();
      case ValidClass::Shaman:
        return new Shaman();
      case ValidClass::Mage:
        return new Mage();
      case ValidClass::Sorcerer:
        return new Sorcerer();
      case ValidClass::Thief:
        return new Thief();
      case ValidClass::Bard:
        return new Bard();
      default:
        throw std::invalid_argument("received invalid character class name");
    }
  };
};
