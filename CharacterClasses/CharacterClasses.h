class Bard : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d6() * level; };

  Bard() : CharacterClass(){};
};

class Cleric : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d8() * level; };

  Cleric() : CharacterClass(){};
};

class Druid : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d8() * level; };

  Druid() : CharacterClass(){};
};

class Fighter : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d10() * level; };

  Fighter() : CharacterClass(){};
};

class Mage : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d4() * level; };

  Mage() : CharacterClass(){};
};

class Monk : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d8() * level; };
  Monk() : CharacterClass(){};
};

class Paladin : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d10() * level; };

  Paladin() : CharacterClass(){};
};

class Ranger : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d10() * level; };

  Ranger() : CharacterClass(){};
};

class Shaman : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d8() * level; };

  Shaman() : CharacterClass(){};
};

class Sorcerer : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d4() * level; };

  Sorcerer() : CharacterClass(){};
};

class Thief : public CharacterClass {
 public:
  int healthPerLevel(int level) { return d6() * level; };

  Thief() : CharacterClass(){};
};
