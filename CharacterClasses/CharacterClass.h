class CharacterClass {
 public:
  virtual int healthPerLevel(int level) = 0;
  CharacterClass(){};
  virtual ~CharacterClass(){};
};
