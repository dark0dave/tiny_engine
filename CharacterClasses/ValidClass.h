enum class ValidClass {
  Fighter,
  Ranger,
  Paladin,
  Cleric,
  Druid,
  Monk,
  Shaman,
  Mage,
  Sorcerer,
  Thief,
  Bard
};
