class Dwarf : public Race {
 public:
  void modifyStats(Stats &stats) {
    stats.constitution += 1;
    stats.dexterity -= 1;
    stats.constitution -= 2;
  }

  Dwarf()
      : Race(4, std::string("Dwarf"),
             {ValidClass::Cleric, ValidClass::Fighter, ValidClass::Thief}){};
};

class Elf : public Race {
 public:
  void modifyStats(Stats &stats) {
    stats.dexterity += 1;
    stats.constitution -= 1;
  }

  Elf()
      : Race(2, "Elf",
             {ValidClass::Cleric, ValidClass::Fighter, ValidClass::Mage,
              ValidClass::Ranger, ValidClass::Sorcerer, ValidClass::Thief}){};
};

class Gnome : public Race {
 public:
  void modifyStats(Stats &stats) {
    stats.dexterity += 1;
    stats.strength -= 1;
    stats.wisdom -= 1;
  }

  Gnome()
      : Race(6, "Gnome",
             {ValidClass::Cleric, ValidClass::Fighter, ValidClass::Thief}){};
};

class HalfElf : public Race {
 public:
  void modifyStats(Stats &){};

  HalfElf()
      : Race(3, "Half Elf",
             {ValidClass::Bard, ValidClass::Cleric, ValidClass::Druid,
              ValidClass::Fighter, ValidClass::Mage, ValidClass::Ranger,
              ValidClass::Shaman, ValidClass::Sorcerer, ValidClass::Thief}){};
};

class Halfling : public Race {
 public:
  void modifyStats(Stats &stats) {
    stats.dexterity += 1;
    stats.strength -= 1;
    stats.wisdom -= 1;
  };

  Halfling()
      : Race(5, "Halfling",
             {ValidClass::Cleric, ValidClass::Fighter, ValidClass::Thief}){};
};

class HalfOrc : public Race {
 public:
  void modifyStats(Stats &stats) {
    stats.strength += 1;
    stats.constitution += 1;
    stats.intelligence -= 2;
  };

  HalfOrc()
      : Race(7, "Half Orc",
             {ValidClass::Cleric, ValidClass::Fighter, ValidClass::Thief,
              ValidClass::Shaman}){};
};

class Human : public Race {
 public:
  void modifyStats(Stats &){};

  Human()
      : Race(1, "Human",
             {ValidClass::Bard, ValidClass::Cleric, ValidClass::Druid,
              ValidClass::Fighter, ValidClass::Mage, ValidClass::Monk,
              ValidClass::Paladin, ValidClass::Ranger, ValidClass::Shaman,
              ValidClass::Sorcerer, ValidClass::Thief}){};
};
