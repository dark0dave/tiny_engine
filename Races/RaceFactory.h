#include "Race.h"
#include "Races.h"

class RaceFactory {
 public:
  static Race *GenerateRace(const ValidRace &choice) {
    switch (choice) {
      case ValidRace::Elf:
        return new Elf();
      case ValidRace::Human:
        return new Human();
      case ValidRace::HalfElf:
        return new HalfElf();
      case ValidRace::Dwarf:
        return new Dwarf();
      case ValidRace::Halfling:
        return new Halfling();
      case ValidRace::Gnome:
        return new Gnome();
      case ValidRace::HalfOrc:
        return new HalfOrc();
      default:
        throw std::invalid_argument("received invalid Race");
    }
  };
};
