#include <set>

class Race {
 public:
  int index;
  std::string name;
  std::set<ValidClass> availableClasses;
  virtual void modifyStats(Stats &stats) = 0;

  Race(int index, std::string name, std::set<ValidClass> availableClasses)
      : index(index), name(name), availableClasses(availableClasses){};

  virtual ~Race() {
    delete[] & availableClasses;
    delete &index;
    delete &name;
  }
};
