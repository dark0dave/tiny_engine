class Stats {
 public:
  int charisma;
  int constitution;
  int dexterity;
  int intelligence;
  int strength;
  int wisdom;
  int level;
  int health;

  Stats(int charisma, int constitution, int dexterity, int intelligence,
        int strength, int wisdom, int level, int heath)
      : charisma(charisma),
        constitution(constitution),
        dexterity(dexterity),
        intelligence(intelligence),
        strength(strength),
        wisdom(wisdom),
        level(level),
        health(heath){};
  Stats() : Stats(0, 0, 0, 0, 0, 0, 1, 1){};
};
